

function throwRock() {
    resetRock();

    var weight = $('#weight').val();
    console.log("Weight = " + weight);

    const hackWeight = 13;

    if(weight == "random") {
        weight = Math.floor(Math.random() * hackWeight) + 1; 
        console.log("Random weight = " + weight)
    } else if (weight == "hack") {
        weight = hackWeight;
    }

    var endingPosition = getDepthPixels(weight);
    var time = getDuration(weight);

    //how long should it take

    $("#stone").animate({ left: endingPosition }, time, "easeOutSine");
}

function stopThrow() {
    $("#stone").stop();
}

function resetRock() {
    $("#stone").stop();
    $("#stone").css("left", "20px");
}

function getDepthPixels(depthSimple) {
    /*
    var depth = "1260px";
    
    if(depthSimple == 'takeout') {
        depth = "1380px";
    } else {
        var depthInt = 1120 + (depthSimple * 20);
        depth = "" + depthInt + "px";
    }
    */
    var depthInt = 1120 + (depthSimple * 20);
    depth = "" + depthInt + "px";

    return depth;
}

function getDuration(depthSimple) {
    var hogToTeeTime = getIceTime();
    var hogToTeeDistance = 93;
    var hackToTeeDistance = 126; //full draw to button/tee distance
    var hackToTeeTime = hogToTeeTime * (hackToTeeDistance / hogToTeeDistance); //need more time to get the full time from hack to the button

    var depthOffset = -1 + ((depthSimple-1) / 6); //estimate is 1 second for every 12 feet, 12 feet = 6 "depths", 4 is top 12, 10 is back 12

    return (hackToTeeTime + depthOffset) * 1000;
}

function getIceTime() {
    //draw from hog to Tee - in seconds
    return 18;
}
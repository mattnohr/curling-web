function drawSheet() {
    const iceColor = "#FFFFFF";
    const ring12Color = "#0404B4";
    const ring8Color = "#FFFFFF";
    const ring4Color = "#B40404";
    const buttonColor = "#FFFFFF";
    const hogLineColor = "#0404B4";

    drawSheetWithColors(iceColor, 
        ring12Color, ring8Color, ring4Color, buttonColor, 
        hogLineColor);
}

function drawSheetWithColors(iceColor, 
    ring12Color, ring8Color, ring4Color, buttonColor, 
    hogLineColor) {
    //sheet
    drawIce(iceColor);

    //left side
    drawHouse(120, ring12Color, ring8Color, ring4Color, buttonColor);

    drawLine(60); //left Back line
    drawLine(120); //left Tee line
    drawLineWithOptions(330, hogLineColor, 2); //left Hog line
    drawHack(0);

    //right side
    drawHouse(1260, ring12Color, ring8Color, ring4Color, buttonColor);

    drawLineWithOptions(1050, hogLineColor, 2); //right Hog line
    drawLine(1260); //right Tee line
    drawLine(1320); //right Back line
    drawHack(1370);

    //center line
    drawCenterLine();
}

function drawIce(iceColor) {
    var ctx = getDrawingContext();
    ctx.fillStyle = iceColor;
    ctx.fillRect(0, 0, 1380, 140);

    //border
    ctx.strokeStyle = "#000000";
    ctx.beginPath();
    ctx.rect(0, 0, 1380, 140);
    ctx.stroke();
}

function drawLine(location) {
    drawLineWithOptions(location, "#585858", 1);
}

function drawLineWithOptions(location, color, thickness) {
    var ctx = getDrawingContext();
    ctx.strokeStyle = color;
    ctx.lineWidth = thickness;
    ctx.beginPath();
    ctx.moveTo(location, 0);
    ctx.lineTo(location, 140);
    ctx.stroke();
}

function drawHouse(center, ring12color, ring8Color, ring4Color, buttonColor) {
    drawCircle(center, 60, ring12color)
    drawCircle(center, 40, ring8Color)
    drawCircle(center, 20, ring4Color)
    drawCircle(center, 8, buttonColor)
}

function drawCircle(center, radius, color) {
    var ctx = getDrawingContext();
    var startAngle = 0;
    var endAngle = 2 * Math.PI;
    var x = center;
    var y = 70;

    ctx.beginPath();
    ctx.arc(x, y, radius, startAngle, endAngle);
    ctx.fillStyle = color;
    ctx.fill();
}

function drawHack(location) {
    var ctx = getDrawingContext();
    var height = 30;
    var width = 10;

    ctx.fillStyle = "#000000";
    ctx.fillRect(location, ((140 / 2) - (height / 2)), width, height);
}

function drawCenterLine() {
    var ctx = getDrawingContext();
    ctx.strokeStyle = "#F78181";
    ctx.beginPath();
    ctx.moveTo(0, 70);
    ctx.lineTo(1380, 70);
    ctx.stroke();
}

function getDrawingContext() {
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");

    return ctx;
}
## Curling Web

This simple application will display a curling sheet using standard HTML canvas tools.

### Curling Dimensions

To draw the sheet, I based it off of the dimensions here:

![curling dimensions](curling_sheet.gif "Curling Dimensions")

I used 10px per 1 foot, so hogline, which is 21' from the Tee line, is 210 pixels away. The complete sheet is 138', or 1380 pixels.

## Calculations

### Slow-down

I used this library to animate the speed of the rock so it slows down at the end: http://gsgd.co.uk/sandbox/jquery/easing/

## GitLab Pages:

This is deployed using GitLab pages.

* Default blank curling sheet: https://mattnohr.gitlab.io/curling-web
* [Customize Colors](https://mattnohr.gitlab.io/curling-web/customize.html)
* [Test Animation](https://mattnohr.gitlab.io/curling-web/animation.html)